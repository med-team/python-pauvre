python-pauvre (0.2.3-4) unstable; urgency=medium

  * syntaxwarning.patch: new: fix SyntaxWarning occurrences. (Closes: #1086899)
  * 2to3.patch: normalize Last-Update timestamp.
  * d/control: declare compliance to standards version 4.7.0.

 -- Étienne Mollier <emollier@debian.org>  Thu, 07 Nov 2024 23:38:31 +0100

python-pauvre (0.2.3-3) unstable; urgency=medium

  * pandas-2.0.patch: new: fix ftbfs with pandas 2.0. (Closes: #1044070)
  * d/copyright: bump packaging copyright year.

 -- Étienne Mollier <emollier@debian.org>  Wed, 06 Sep 2023 22:18:51 +0200

python-pauvre (0.2.3-2) unstable; urgency=medium

  * biopython-1.80.patch: add; migrate away from Bio.SubsMat.
    This patch fixes build time test failures with Biopython 1.80 caused
    by removal of Bio.SubsMat in favor of Bio.Align.substitution_matrices.
    (Closes: 1024835)
  * d/control: declare compliance to standards version 4.6.2.
  * d/copyright: update debian/ authors and year.

 -- Étienne Mollier <emollier@debian.org>  Sun, 18 Dec 2022 15:56:49 +0100

python-pauvre (0.2.3-1) unstable; urgency=medium

  [ Nilesh Patra ]
  * Team Upload.
  * New upstream version 0.2.3
  * Declare compliance with policy 4.5.1

  [ Etienne Mollier ]
  * d/control: updated my uploader address

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 18:47:57 +0530

python-pauvre (0.2.2-2) unstable; urgency=medium

  * Added myself as uploader.
  * Added (build-)dependency on python3-tk (Closes: #962698)
  * Cleanup test data after build time tests; this reduces the weight of the
    python3-pauvre package, and improve reproducible builds.
  * Added ref to bioconda (Steffen Moeller)

 -- Étienne Mollier <etienne.mollier@mailoo.org>  Wed, 08 Jul 2020 18:40:39 +0200

python-pauvre (0.2.2-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 11 Jun 2020 22:36:46 +0200

python-pauvre (0.2.1-1) unstable; urgency=medium

  * Initial release (Closes: #958951)

 -- Andreas Tille <tille@debian.org>  Sat, 02 May 2020 10:08:54 +0200
