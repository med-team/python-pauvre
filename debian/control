Source: python-pauvre
Section: science
Priority: optional
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
               python3-setuptools,
               python3-matplotlib <!nocheck>,
               python3-biopython <!nocheck>,
               python3-pandas <!nocheck>,
               python3-numpy <!nocheck>,
               python3-scipy <!nocheck>,
               python3-sklearn <!nocheck>,
               python3-tk <!nocheck>,
               python3-progressbar <!nocheck>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/python-pauvre
Vcs-Git: https://salsa.debian.org/med-team/python-pauvre.git
Homepage: https://github.com/conchoecia/pauvre
Rules-Requires-Root: no

Package: python3-pauvre
Architecture: all
Section: python
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-tk,
         python3-matplotlib,
         python3-biopython,
         python3-pandas,
         python3-numpy,
         python3-scipy,
         python3-sklearn
Description: QC and genome browser plotting Oxford Nanopore and PacBio long reads
 Pauvre is a plotting package designed for nanopore and PacBio long reads.
 .
 This package currently hosts four scripts for plotting and/or printing stats.
 .
  pauvre marginplot
     Takes a fastq file as input and outputs a marginal histogram with a
     heatmap.
  pauvre stats
     Takes a fastq file as input and prints out a table of stats, including
     how many basepairs/reads there are for a length/mean quality cutoff.
     This is also automagically called when using pauvre marginplot
  pauvre redwood
     Method of representing circular genomes. A redwood plot contains long
     reads as "rings" on the inside, a gene annotation "cambrium/phloem",
     and a RNAseq "bark". The input is .bam files for the long reads and
     RNAseq data, and a .gff file for the annotation.
  pauvre synteny
     Makes a synteny plot of circular genomes. Finds the most parsimonius
     rotation to display the synteny of all the input genomes with the
     fewest crossings-over. Input is one .gff file per circular genome
     and one directory of gene alignments.
